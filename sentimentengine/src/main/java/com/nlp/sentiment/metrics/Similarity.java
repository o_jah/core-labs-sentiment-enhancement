package com.nlp.sentiment.metrics;

public interface Similarity<T> {

	public double similarityScore(T u, T v);
}
