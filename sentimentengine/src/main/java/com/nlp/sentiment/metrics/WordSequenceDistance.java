package com.nlp.sentiment.metrics;

import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;

import com.nlp.sentiment.utils.ContentFeatureExtractor;

public class WordSequenceDistance implements Distance<String> {

	public double getDistance(String str1, String str2) {

		Map<String, ? extends Number> featureMap1 = ContentFeatureExtractor
				.getFeatureMap(str1);
		Map<String, ? extends Number> featureMap2 = ContentFeatureExtractor
				.getFeatureMap(str2);

		if (MapUtils.isEmpty(featureMap1) || MapUtils.isEmpty(featureMap2)) {
			return 0;
		}

		Set<String> set = featureMap1.keySet();
		String[] array1 = set.toArray(new String[set.size()]);

		set = featureMap2.keySet();
		String[] array2 = set.toArray(new String[set.size()]);

		if (array1 == null || array2 == null) {
			return 0;
		}

		int i = 0, j = 0;
		double distance = 0;

		while (i < array1.length || j < array2.length) {

			if (i > array1.length || !array1[i].equalsIgnoreCase(array2[j])) {
				j++;
			} else if (j > array2.length
					|| !array1[i].equalsIgnoreCase(array2[j])) {
				i++;
			} else {
				distance += ((featureMap1.get(array1[i]).doubleValue() * featureMap1
						.get(array1[i]).doubleValue()) - (featureMap2.get(
						array2[j]).doubleValue() * featureMap2.get(array2[j])
						.doubleValue()));
				i++;
				j++;
			}
		}
		return Math.sqrt(distance);
	}
}
