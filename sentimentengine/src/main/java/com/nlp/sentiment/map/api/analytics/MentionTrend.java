package com.nlp.sentiment.map.api.analytics;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "mention_trend")
public class MentionTrend {
	@XmlElement(name = "mention")
	protected List<MentionInfo> mentionTrend;

	public List<MentionInfo> getMentionTrend() {
		return mentionTrend;
	}

	public void setMentionTrend(List<MentionInfo> mentionTrend) {
		this.mentionTrend = mentionTrend;
	}

}
