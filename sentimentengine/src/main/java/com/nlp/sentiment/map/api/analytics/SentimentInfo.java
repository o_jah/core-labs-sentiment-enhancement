/**
 * 
 */
package com.nlp.sentiment.map.api.analytics;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "overall_sentiment")
@XmlAccessorType(XmlAccessType.FIELD)
public class SentimentInfo {
	@XmlAttribute
	protected float neutral;
	@XmlAttribute
	protected float positive;
	@XmlAttribute
	protected float negative;

	public SentimentInfo() {

	}

	public SentimentInfo(float positive, float negative) {
		this.positive = positive;
		this.negative = negative;
	}

	public float getPositive() {
		return positive;
	}

	public void setPositive(float positive) {
		this.positive = positive;
	}

	public float getNegative() {
		return negative;
	}

	public void setNegative(float negative) {
		this.negative = negative;
	}

	public void setNeutral(float neutral) {
		this.neutral = neutral;
	}

	public float getNeutral() {
		setNeutral((10000 - positive * 100 - negative * 100) / 100.0f);
		return neutral;
	}

}
