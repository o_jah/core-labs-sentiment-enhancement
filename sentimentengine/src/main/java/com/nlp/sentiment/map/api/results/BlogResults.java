package com.nlp.sentiment.map.api.results;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "blog_results")
@XmlAccessorType(XmlAccessType.FIELD)
public class BlogResults extends BasicResults {

}
