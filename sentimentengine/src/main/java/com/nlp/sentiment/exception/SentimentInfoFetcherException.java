package com.nlp.sentiment.exception;

public class SentimentInfoFetcherException extends Exception {

	private static final long serialVersionUID = 1102303700816863227L;

	public SentimentInfoFetcherException(Throwable cause) {
		super(cause);
	}

	public SentimentInfoFetcherException(String message) {
		super(message);
	}

	public SentimentInfoFetcherException(Exception e) {
		super(e);
	}
}
