package com.nlp.sentiment.utils;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter1 extends XmlAdapter<String, Date> {

	public Date unmarshal(String value) {
		return (XMLDateAdapter.parseDate(value));
	}

	public String marshal(Date value) {
		return (XMLDateAdapter.printDate(value));
	}

}