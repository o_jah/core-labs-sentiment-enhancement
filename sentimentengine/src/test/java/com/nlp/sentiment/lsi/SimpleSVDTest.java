package com.nlp.sentiment.lsi;

import static org.junit.Assert.assertTrue;

import org.ejml.simple.SimpleMatrix;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.nlp.sentiment.exception.LSIException;

public class SimpleSVDTest {

	private SimpleSVD<SimpleMatrix> simpleSVD;
	private static final double[][] matrix = { { 3, 2, 2 }, { 2, 3, -2 } };

	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() throws LSIException {
		simpleSVD = new SimpleSVD<SimpleMatrix>(matrix, false);
	}

	@Test
	public void svdTest() {
		assertTrue(simpleSVD.getU() != null);
		assertTrue(simpleSVD.getV() != null);
		assertTrue(simpleSVD.getW() != null);

		System.out.println("**************************");
		
		System.out.println(simpleSVD.getU());
		System.out.println(simpleSVD.getV());
		System.out.println(simpleSVD.getW());
	}
}
