package com.nlp.sentiment.enhance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.beust.jcommander.JCommander;
import com.nlp.sentiment.enhance.EnhancementMain.Parameters;
import com.nlp.sentiment.enhance.logic.LPSentimentEnhancer;
import com.nlp.sentiment.exception.LPInfluencerGraphBuilderException;
import com.nlp.sentiment.exception.SentimentEnhancerException;
import com.nlp.sentiment.exception.SentimentInfoFetcherException;
import com.nlp.sentiment.label.prop.graph.LPInfluencerGraphBuilder;
import com.sysomos.core.search.transfer.TweetVO;
import com.sysomos.nlp.sentiment.Sentiment;

public class EnhancementMainTest {

	private Parameters params;
	private EnhancementMain enhancer;
	private static final List<String> QUERIES = new ArrayList<String>();
	private static final List<String> ARG_QUERIES = new ArrayList<String>();

	static {
		QUERIES.add("Snow");
		QUERIES.add("Harsh winters");
		QUERIES.add("Snowmaggedon");

		// QUERIES.add("2014 or climate change");
		// QUERIES.add("Advent or Brazil");
		// QUERIES.add("America or heart");
		// QUERIES.add("Android or Chinese");
		// QUERIES.add("Apple or America");
		// QUERIES.add("Big Data or Independence");
		// QUERIES.add("Chinese or Localisation");
		// QUERIES.add("Christmas or new years");
		// QUERIES.add("Climate change or ice");
		// QUERIES.add("bucket challenge");
		// QUERIES.add("Cosby or fashion");
		// QUERIES.add("Crimea or heart");
		// QUERIES.add("Emoji or chinese");
		// QUERIES.add("Engagement or Resolution");
		// QUERIES.add("FIFA or USA");
		// QUERIES.add("Global warming or Hacking");
		// QUERIES.add("Hacking or Ebola");
		// QUERIES.add("Hashtag or Putin");
		// QUERIES.add("Ice bucket challenge or cosby");
		// QUERIES.add("Iphone or Global Warming");
		// QUERIES.add("Katy Perry or vape");
		// QUERIES.add("Ipad");

		// QUERIES.add("Super Bowl 2015");
		// QUERIES.add("Seahawks");
		// QUERIES.add("Russell Wilson");

		// -------------------------------------------------- COMMAND LINE ARGS

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-apiKey",
				"2f86dd305c37afe2a23653250fb77179" })); // my key

		for (String query : QUERIES) {
			ARG_QUERIES.addAll(Arrays.asList(new String[] { "-query", query }));
		}

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-method", "1" }));

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-contentType", "1" }));

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-startDateMillis",
				String.valueOf(new DateTime().minusDays(5).getMillis()) }));

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-endDateMillis",
				String.valueOf(new DateTime().minusDays(1).getMillis()) }));
	}

	private static final String ARGS[] = ARG_QUERIES
			.toArray(new String[ARG_QUERIES.size()]);

	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {
		enhancer = new EnhancementMain();
		params = new Parameters();
		new JCommander(params, ARGS).parse();
	}

	@Test
	public void getResultsTest() {
		try {
			Set<SentimentResults> sentimentResultSet = enhancer
					.getSentiments(params);
			System.out.println("**************************");
			for (SentimentResults sentiment : sentimentResultSet) {
				StringBuilder builder = new StringBuilder();
				builder.append("Query: ").append(sentiment.getQuery())
						.append(" - ");
				builder.append("Positive Sentiments: ")
						.append(sentiment.getPosList().size()).append(" - ");
				builder.append("Negative Sentiments: ")
						.append(sentiment.getNegList().size()).append(" - ");
				builder.append("Neutral Sentiments: ")
						.append(sentiment.getNeutralList().size())
						.append(" - ");
				builder.append("None Sentiments: ").append(
						sentiment.getNoneList().size());
				System.out.println("Classification - [ " + builder.toString()
						+ " ]");
			}
			System.out.println("**************************");
		} catch (SentimentInfoFetcherException e) {
			System.out.println(e.getLocalizedMessage());
		}
	}

	@Test
	public void getResultsXMLFormatTest() {
		Map<String, String> searchResults = enhancer
				.getResultsXMLFormat(params);
		System.out.println();
		System.out.println("**************************");
		System.out.println("Results Size " + searchResults.size());

		for (Map.Entry<String, String> entry : searchResults.entrySet()) {
			System.out.println();
			System.out.println("**************************");
			System.out.println("Query: " + entry.getKey());
			System.out.println("XML Content: " + entry.getValue());
			System.out.println("**************************");
		}
	}

	@Test
	public void getWeights() {
		for (String query : params.queries) {
			List<TweetVO> tweets = enhancer.getMatchingTweets(query,
					params.apiKey, params.contentType, params.startDateMillis,
					params.endDateMillis);
			Map<Long, SentimentResults> userSentMap = enhancer
					.getTweetSentiments(query, tweets);
			Map<Long, Sentiment> userIdS = new HashMap<Long, Sentiment>();
			for (Long userId : userSentMap.keySet()) {
				SentimentResults results = userSentMap.get(userId);

				List<String> content = results.getContent();

				if (CollectionUtils.isEmpty(content)) {
					continue;
				}

				System.out.println("**************************");
				System.out.println("Tweets Size " + content.size());

				double[][] weights = enhancer.weight(content);
				System.out.println("**************************");
				System.out.println("User ID: " + userId.longValue());
				System.out.println("Weights: ");

				if (weights != null) {
					for (int i = 0; i < weights.length; i++) {
						String str = "";
						if (weights[i] == null) {
							str += " - ";
							continue;
						}
						int j;
						for (j = 0; j < weights[i].length; j++) {
							if (j < weights[i].length - 1) {
								str += weights[i][j] + ", ";
								continue;
							}
							str += weights[i][j];
						}
						System.out.println(str);
					}
				}
				List<String> posList = results.getPosList();
				List<String> negList = results.getNegList();
				List<String> neutralList = results.getNeutralList();
				int size = posList == null ? 0 : posList.size();
				System.out.println("Positive Labels: " + size);
				size = negList == null ? 0 : negList.size();
				System.out.println("Negative Labels: " + size);
				size = neutralList == null ? 0 : neutralList.size();
				System.out.println("Neutral  Labels: " + size);
				try {
					Sentiment sentiment = enhancer.labelUserTweets(results, 1);
					userIdS.put(userId, sentiment);
					System.out.println("Sentiment: " + sentiment);
				} catch (SentimentEnhancerException e) {
					System.out.println(e.getLocalizedMessage());
				}
			}
			System.out.println("**************************");
			int posS = 0, negS = 0, neuS = 0, nonS = 0;
			for (Map.Entry<Long, Sentiment> idS : userIdS.entrySet()) {
				if (Sentiment.POS == idS.getValue()) {
					posS++;
				} else if (Sentiment.NEG == idS.getValue()) {
					negS++;
				} else if (Sentiment.NEUTRAL == idS.getValue()) {
					neuS++;
				} else {
					nonS++;
				}
			}
			StringBuilder builder = new StringBuilder();
			builder.append("Query: ").append(query).append(" - ");
			builder.append("Positive Users: ").append(posS).append(" - ");
			builder.append("Negative Users: ").append(negS).append(" - ");
			builder.append("Neutral Users: ").append(neuS).append(" - ");
			builder.append("None Users: ").append(nonS);
			System.out.println("Classification - [ " + builder.toString()
					+ " ]");
			try {
				userIdS = enhancer.reorganize(userIdS);
				double[][] transWeights = LPInfluencerGraphBuilder.build(
						tweets, userIdS);
				double[][] classProbs = enhancer.classProbs(query,
						params.apiKey, params.startDateMillis,
						params.endDateMillis, params.contentType,
						params.method, userIdS.size());

				LPSentimentEnhancer lpEnhancer = new LPSentimentEnhancer(
						classProbs, transWeights);
				Map<Integer, Sentiment> labels = lpEnhancer.label(null);

				if (MapUtils.isEmpty(labels) || labels.size() != userIdS.size()) {
					int lSize = MapUtils.isEmpty(labels) ? 0 : labels.size();
					System.out.println("Unexpected error. Labels : " + lSize
							+ ", Users: " + userIdS.size());
					return;
				}
				String output = "Labels: [ ";
				List<Long> ids = new ArrayList<Long>(userIdS.keySet());
				for (Map.Entry<Integer, Sentiment> entry : labels.entrySet()) {
					long id = ids.get(entry.getKey());
					output += id + ": " + entry.getValue() + " ";
				}
				output += "]";
				System.out.println(output);
			} catch (LPInfluencerGraphBuilderException e) {
				System.out.println(e.getLocalizedMessage());
			} catch (SentimentEnhancerException e) {
				System.out.println(e.getLocalizedMessage());
			}
		}
	}

	@After
	public void tearDown() {
		params = null;
		enhancer = null;
	}
}
