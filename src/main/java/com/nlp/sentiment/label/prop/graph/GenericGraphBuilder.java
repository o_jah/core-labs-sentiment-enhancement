package com.nlp.sentiment.label.prop.graph;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

import com.nlp.sentiment.utils.Pair;
import com.sysomos.sax.service.transfer.InfluencerGraph;

public class GenericGraphBuilder {

	public static InfluencerGraph build(String filepath, Set<Long> userIds)
			throws IOException {

		Map<Pair<Long, Long>, Double> edgeMap = new HashMap<Pair<Long, Long>, Double>();
		FileReader reader = new FileReader(new File(filepath));
		CSVReader csvReader = new CSVReader(reader);
		String[] values = csvReader.readNext();

		while (values != null) {
			Pair<Long, Long> pair = new Pair<Long, Long>(
					Long.valueOf(values[0]), Long.valueOf(values[1]));
			edgeMap.put(pair, Double.valueOf(values[2]));
			values = csvReader.readNext();
		}
		csvReader.close();

		InfluencerGraph graph = new InfluencerGraph();
		Set<InfluencerGraph.GraphNode> nodes = new HashSet<InfluencerGraph.GraphNode>();
		List<InfluencerGraph.GraphEdge> edges = new ArrayList<InfluencerGraph.GraphEdge>();
		for (long source : userIds) {
			for (long target : userIds) {
				Pair<Long, Long> pair = new Pair<Long, Long>(source, target);
				if (edgeMap.containsKey(pair)) {
					nodes.add(new InfluencerGraph.GraphNode(source));
					nodes.add(new InfluencerGraph.GraphNode(target));

					InfluencerGraph.GraphEdge edge = new InfluencerGraph.GraphEdge();
					edge.source = String.valueOf(source);
					edge.target = String.valueOf(target);
					edge.weight = edgeMap.get(pair);

					edges.add(edge);
				}
			}
		}
		graph.nodes = new ArrayList<InfluencerGraph.GraphNode>(nodes);
		graph.edges = edges;

		List<String> diff = new ArrayList<String>();
		for (long id : userIds) {
			diff.add(String.valueOf(id));
		}
		diff.removeAll(graph.nodes);

		System.out.println("Before: " + userIds.size() + " - Diff Size: "
				+ (diff == null ? 0 : diff.size()));

		return graph;
	}
}
