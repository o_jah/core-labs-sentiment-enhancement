package com.nlp.sentiment.label.prop.graph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.nlp.sentiment.exception.LPInfluencerGraphBuilderException;
import com.sysomos.core.search.transfer.TweetVO;
import com.sysomos.nlp.sentiment.Sentiment;
import com.sysomos.sax.service.InfluencerGraphService;
import com.sysomos.sax.service.transfer.InfluencerGraph;
import com.sysomos.sax.service.transfer.Request;
import com.sysomos.sax.service.transfer.Tweet;

public class LPInfluencerGraphBuilder {

	/**
	 * @param map
	 *            Map of user-sentiment
	 * @return a reorganized map of values where entries with positive
	 *         sentiments appear first, followed by entries with negative
	 *         sentiments, then entries with neutral or none sentiments.
	 */
	private static Map<Long, Sentiment> reorganize(Map<Long, Sentiment> map) {
		if (MapUtils.isEmpty(map)) {
			return map;
		}
		Map<Long, Sentiment> sortedMap = new HashMap<Long, Sentiment>();
		Map<Long, Sentiment> negative = new HashMap<Long, Sentiment>();
		Map<Long, Sentiment> remaining = new HashMap<Long, Sentiment>();

		for (Map.Entry<Long, Sentiment> entry : map.entrySet()) {
			if (entry == null) {
				continue;
			}
			if (Sentiment.POS == entry.getValue()) {
				sortedMap.put(entry.getKey(), entry.getValue());
			} else if (Sentiment.NEG == entry.getValue()) {
				negative.put(entry.getKey(), entry.getValue());
			} else {
				remaining.put(entry.getKey(), entry.getValue());
			}
		}
		sortedMap.putAll(negative);
		sortedMap.putAll(remaining);

		return sortedMap;
	}

	public static double[][] build(final List<TweetVO> tweets,
			final List<String> handles, Map<Long, Sentiment> sentiments)
			throws LPInfluencerGraphBuilderException {
		return build(tweets, handles, sentiments, null);
	}

	public static double[][] build(final List<TweetVO> tweets,
			final List<String> handles, Map<Long, Sentiment> sentiments,
			String filepath) throws LPInfluencerGraphBuilderException {
		validate(tweets, handles, sentiments);
		sentiments = reorganize(sentiments);
		List<Tweet> adaptedTweets = adapt(tweets);
		InfluencerGraphService graphService = new InfluencerGraphService();
		Request.Filter filter = new Request.Filter();
		filter.verifiedAccount = true;
		if (CollectionUtils.isEmpty(adaptedTweets)) {
			System.out.println("Empty handles");
		}

		InfluencerGraph graph;
		if (StringUtils.isEmpty(filepath)) {
			graph = graphService.findInfluencers(adaptedTweets, handles,
					handles.size(), handles.size(), false, filter);
		} else {
			try {
				graph = GenericGraphBuilder
						.build(filepath, sentiments.keySet());
			} catch (IOException e) {
				graph = null;
			}
		}
		if (graph == null) {
			throw new LPInfluencerGraphBuilderException(
					"Couldn't get influencer graph from list of tweets.");
		}
		List<InfluencerGraph.GraphEdge> edges = graph.edges;
		if (edges == null) {
			throw new LPInfluencerGraphBuilderException(
					"No edges in the graph. ");
		}

		double[][] weights = new double[sentiments.size()][sentiments.size()];

		for (int i = 0; i < weights.length; i++) {
			for (int j = i; j < weights.length; j++) {
				if (i == j) {
					weights[i][j] = 1.0;
				} else {
					weights[i][j] = 0.0;
					weights[j][i] = 0.0;
				}
			}
		}

		List<Long> ids = new ArrayList<Long>(sentiments.keySet());
		for (InfluencerGraph.GraphEdge edge : edges) {
			Long sourceId = Long.valueOf(edge.source);
			Long targetId = Long.valueOf(edge.target);
			if (sentiments.containsKey(sourceId)
					&& sentiments.containsKey(targetId)) {
				int i = ids.indexOf(sourceId);
				int j = ids.indexOf(targetId);
				weights[i][j] = edge.weight;
			}
		}
		return smooth(weights);
	}

	/**
	 * Add a Laplacian correction to the weights so that the propagation can
	 * work.
	 * 
	 * @param weights
	 * @return
	 */
	private static double[][] smooth(double[][] weights) {
		if (weights == null) {
			return weights;
		}

		for (int i = 0; i < weights.length; i++) {
			if (weights[i] == null)
				continue;
			double factor = 1.0 / weights[i].length;
			for (int j = 0; j < weights[i].length; j++) {
				weights[i][j] += factor;
			}
		}
		return weights;
	}

	private static void validate(final List<TweetVO> tweets,
			final List<String> handles, final Map<Long, Sentiment> sentiments)
			throws LPInfluencerGraphBuilderException {
		if (MapUtils.isEmpty(sentiments) || CollectionUtils.isEmpty(tweets)
				|| CollectionUtils.isEmpty(handles)) {
			throw new LPInfluencerGraphBuilderException("");
		}

		boolean unLabeledOnly = true, positiveOnly = true, negativeOnly = true;
		for (Map.Entry<Long, Sentiment> entry : sentiments.entrySet()) {
			positiveOnly = positiveOnly && (Sentiment.POS == entry.getValue());
			negativeOnly = negativeOnly && (Sentiment.NEG == entry.getValue());
			unLabeledOnly = unLabeledOnly
					&& (Sentiment.NEUTRAL == (entry.getValue()) || (Sentiment.NONE == entry
							.getValue()));
		}

		if (unLabeledOnly || positiveOnly || negativeOnly) {
			String cause = "Input map does not satisfy LP algorithm requirements. "
					+ "Map should contain positive or negative, and unlabeled instances.";
			throw new LPInfluencerGraphBuilderException(cause);
		}
	}

	private static List<Tweet> adapt(List<TweetVO> tweetsVO) {
		if (CollectionUtils.isEmpty(tweetsVO))
			return null;
		List<Tweet> adaptedTweets = new ArrayList<Tweet>();
		for (TweetVO aTweet : tweetsVO) {
			Tweet tw = new Tweet();
			tw.author = aTweet.getActorName();
			tw.createdTs = aTweet.getCreateDate();
			tw.screenName = aTweet.getActorScreenName();
			tw.text = aTweet.getContents();
			tw.tweetId = String.valueOf(aTweet.getDocId());
			tw.userId = aTweet.getActorId();
			adaptedTweets.add(tw);
		}
		return adaptedTweets;
	}
}
