package com.nlp.sentiment.exception;

public class LPInfluencerGraphBuilderException extends Exception {

	private static final long serialVersionUID = -5767414327834546453L;

	public LPInfluencerGraphBuilderException(Throwable cause) {
		super(cause);
	}

	public LPInfluencerGraphBuilderException(String message) {
		super(message);
	}

	public LPInfluencerGraphBuilderException(Exception e) {
		super(e);
	}
}
