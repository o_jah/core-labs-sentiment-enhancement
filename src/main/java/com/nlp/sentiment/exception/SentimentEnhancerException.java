package com.nlp.sentiment.exception;

public class SentimentEnhancerException extends Exception {

	private static final long serialVersionUID = -617946419515559166L;

	public SentimentEnhancerException(Throwable cause) {
		super(cause);
	}

	public SentimentEnhancerException(String message) {
		super(message);
	}

	public SentimentEnhancerException(Exception e) {
		super(e);
	}
}
