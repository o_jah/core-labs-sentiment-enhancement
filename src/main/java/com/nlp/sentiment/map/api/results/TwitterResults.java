package com.nlp.sentiment.map.api.results;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "twitter_results")
@XmlAccessorType(XmlAccessType.FIELD)
public class TwitterResults extends BasicResults {

}
