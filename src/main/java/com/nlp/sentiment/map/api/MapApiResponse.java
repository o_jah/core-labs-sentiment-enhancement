package com.nlp.sentiment.map.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

import com.nlp.sentiment.map.api.results.BlogResults;
import com.nlp.sentiment.map.api.results.ForumResults;
import com.nlp.sentiment.map.api.results.NewsResults;
import com.nlp.sentiment.map.api.results.TwitterResults;

@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
public class MapApiResponse {
	@XmlElement(name = "request")
	private String request;
	@XmlElementRef(name = "twitter_results")
	private TwitterResults twitterResults;
	@XmlElementRef(name = "blog_results")
	private BlogResults blogResults;
	@XmlElementRef(name = "forum_results")
	private ForumResults forumResults;
	@XmlElementRef(name = "news_results")
	private NewsResults newsResults;

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public TwitterResults getTwitterResults() {
		return twitterResults;
	}

	public void setTwitterResults(TwitterResults twitterResults) {
		this.twitterResults = twitterResults;
	}

	public BlogResults getBlogResults() {
		return blogResults;
	}

	public void setBlogResults(BlogResults blogResults) {
		this.blogResults = blogResults;
	}

	public ForumResults getForumResults() {
		return forumResults;
	}

	public void setForumResults(ForumResults forumResults) {
		this.forumResults = forumResults;
	}

	public NewsResults getNewsResults() {
		return newsResults;
	}

	public void setNewsResults(NewsResults newsResults) {
		this.newsResults = newsResults;
	}
}
