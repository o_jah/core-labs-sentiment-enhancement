package com.nlp.sentiment.metrics;


public interface Distance<T> {

	public double getDistance(T u, T v);
}
