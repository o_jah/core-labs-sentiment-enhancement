package com.nlp.sentiment.lsi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.ejml.data.DenseMatrix64F;
import org.ejml.simple.SimpleMatrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aliasi.matrix.CosineDistance;
import com.aliasi.matrix.SparseFloatVector;
import com.aliasi.matrix.Vector;
import com.nlp.sentiment.exception.LSIException;
import com.nlp.sentiment.utils.ContentFeatureExtractor;

public class LSI {

	/**
	 * Dimensionality reduction factor
	 */
	private static final int DIMENSION = 100;
	private List<Map<String, ? extends Number>> docFeatures;
	private static final Logger LOG = LoggerFactory.getLogger(LSI.class);

	public LSI(final List<String> documents) {
		docFeatures = featureListMap(documents);
	}

	public Map<Integer, Double> queryDocsRelevance(String query)
			throws LSIException {
		if (StringUtils.isEmpty(query)) {
			return null;
		}

		Map<String, ? extends Number> queryFeatures = featureMap(query);
		List<Map<String, ? extends Number>> docQFeatures;
		docQFeatures = new ArrayList<Map<String, ? extends Number>>(docFeatures);
		docQFeatures.add(queryFeatures);
		Set<String> featureSet = featureSet(docQFeatures);

		double[][] docsValues = features(featureSet, docFeatures);
		SimpleSVD<SimpleMatrix> simpleSVD = new SimpleSVD<SimpleMatrix>(
				new DenseMatrix64F(docsValues), false);

		SimpleMatrix U = simpleSVD.getU();
		SimpleMatrix V = simpleSVD.getV();
		SimpleMatrix W = simpleSVD.getW();

		int rank = simpleSVD.rank();
		rank = rank <= DIMENSION ? rank : DIMENSION;

		double[] qFeatureVals = featureValues(featureSet, queryFeatures);
		SimpleMatrix product = new SimpleMatrix(new double[][] { qFeatureVals });

		System.out.println("Q : " + " [ row: " + product.numRows() + ", col: "
				+ product.numCols() + " ] ");
		System.out.println("U : " + " [ row: " + U.numRows() + ", col: "
				+ U.numCols() + " ] ");
		System.out.println("V : " + " [ row: " + V.numRows() + ", col: "
				+ V.numCols() + " ] ");
		System.out.println("W : " + " [ row: " + W.numRows() + ", col: "
				+ W.numCols() + " ] ");
		
		// The query needs to be reduced in dimension (to size rank)
		product = product.mult(U.extractMatrix(0, U.numRows(), 0, rank));
		product = product.mult(W.extractMatrix(0, rank, 0, rank).invert());
		product = product.transpose();
		System.out.println("Q': " + " [ row: " + product.numRows() + ", col: "
				+ product.numCols() + " ] ");

		qFeatureVals = new double[product.numRows()];
		for (int i = 0; i < product.numRows(); i++) {
			qFeatureVals[i] = product.get(i, 0);
		}

		// V needs to be reduced in dimension (to size rank) as well
		SimpleMatrix newV = V.extractMatrix(0, rank, 0, V.numCols());

		System.out.println("V': " + " [ row: " + newV.numRows() + ", col: "
				+ newV.numCols() + " ] ");

		int[] arrayKey = new int[newV.numRows()];
		double[] columnVector = new double[newV.numRows()];
		Map<Integer, Double> relevance = new HashMap<Integer, Double>();

		for (int j = 0; j < newV.numCols(); j++) {
			for (int i = 0; i < newV.numRows(); i++) {
				arrayKey[i] = i;
				columnVector[i] = newV.get(i, j);
			}
			try {
				System.out.println("A : " + arrayKey.length + ", B : "
						+ columnVector.length + ", C : " + qFeatureVals.length);
				double score = docQRelScore(arrayKey, columnVector,
						qFeatureVals);
				relevance.put(j, score);
			} catch (LSIException e) {
				System.out.println(e.getLocalizedMessage());
				LOG.error(e.getLocalizedMessage(), e);
			}
		}
		Map<Integer, Double> sortedRel = new TreeMap<Integer, Double>(
				Collections.reverseOrder());
		sortedRel.putAll(relevance);
		return sortedRel;
	}

	private double docQRelScore(final int[] keys, double[] lhs, double[] rhs)
			throws LSIException {
		if (lhs == null || rhs == null || lhs.length != rhs.length) {
			throw new LSIException(
					"Cosine Similarity works on vectors of same size.");
		}
		Vector vLhs = new SparseFloatVector(keys, convert(lhs), keys.length);
		Vector vRhs = new SparseFloatVector(keys, convert(rhs), keys.length);
		return new CosineDistance().proximity(vLhs, vRhs);
	}

	private float[] convert(double array[]) {
		if (array == null)
			return null;
		float[] floats = new float[array.length];
		for (int i = 0; i < array.length; i++) {
			floats[i] = (float) array[i];
		}
		return floats;
	}

	private Map<String, ? extends Number> featureMap(String doc) {
		return ContentFeatureExtractor.getFeatureMap(doc);
	}

	private List<Map<String, ? extends Number>> featureListMap(
			List<String> documents) {
		if (CollectionUtils.isEmpty(documents)) {
			return null;
		}
		List<Map<String, ? extends Number>> docFeatures;
		docFeatures = new ArrayList<Map<String, ? extends Number>>();
		for (String doc : documents) {
			docFeatures.add(featureMap(doc));
		}
		return docFeatures;
	}

	private Set<String> featureSet(
			final List<Map<String, ? extends Number>> docFeatures) {

		if (CollectionUtils.isEmpty(docFeatures)) {
			return null;
		}

		Set<String> totalFeatures = new HashSet<String>();
		for (Map<String, ? extends Number> featureMap : docFeatures) {
			if (MapUtils.isEmpty(featureMap)) {
				continue;
			}
			totalFeatures.addAll(featureMap.keySet());
		}

		return totalFeatures;
	}

	private double[] featureValues(final Set<String> featureSet,
			final Map<String, ? extends Number> featureMap) {
		if (CollectionUtils.isEmpty(featureSet) || MapUtils.isEmpty(featureMap)) {
			return null;
		}
		double[] featureArray = new double[featureSet.size()];
		populateArray(featureArray, featureSet, featureMap);
		return featureArray;
	}

	private static void populateArray(double[] featureArray,
			final Collection<String> featureSet,
			final Map<String, ? extends Number> featureMap) {

		if (featureArray == null) {
			return;
		}

		int acc = 0, arrayKey[] = new int[featureSet.size()];
		for (String feature : featureSet) {
			arrayKey[acc] = acc;
			int count1 = 0;
			if (featureMap.containsKey(feature)) {
				count1 = featureMap.get(feature).intValue();
			}
			featureArray[acc] = count1;
			acc++;
		}
	}

	private double[][] features(final Set<String> featureSet,
			final List<Map<String, ? extends Number>> docFeatures) {
		int index = 0;
		double[][] features = new double[docFeatures.size()][featureSet.size()];
		for (Map<String, ? extends Number> featureMap : docFeatures) {
			double[] featureArray = featureValues(featureSet, featureMap);
			features[index] = featureArray;
			index++;
		}
		return transpose(features);
	}

	private double[][] transpose(final double[][] array) {
		if (array == null) {
			return null;
		}

		double[][] transpose = new double[array[0].length][array.length];
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				transpose[j][i] = array[i][j];
			}
		}

		return transpose;
	}

}
